###File System manager
This program can be used gather information about the files 
in the resource folder. It handles user input to manipulate 
the files in the following ways:

* Print all file names in resource folder
* Print file names in resource folder by extension
* Print Dracula.txt file name
* Print Dracula.txt file size
* Print amount of lines in Dracula.txt
* Search if Dracula.txt contains specific word
* Search how many times word appears in Dracula.txt

Project can be found at: [GitLab](https://gitlab.com/SarahMags/filesysmanager)
