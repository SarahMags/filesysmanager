package main.java.no.noroff;

import java.util.Scanner;

public class ConsoleMenu { //Class presents menu options and receives user choice
    static Scanner ioScanner = new Scanner(System.in);
    static int menuNr;

    static int progMenu(){
        System.out.println("""
                Enter number to choose action:
                1:Print all file names\s
                2:Print file names by extension
                3:Print Dracula file name\s
                4:Print Dracula file size
                5:Print amount of lines in Dracula\s
                6:Search if Dracula contains specific word
                7:Search how many times word appears in Dracula
                8:Quit""");
        do{
            while(!ioScanner.hasNextInt()){
                System.out.println("Please enter a valid number.");
                ioScanner.next(); //keeps the scanner going until it has the next valid int
            }
            menuNr = ioScanner.nextInt();
        } while(menuNr < 0);

        return menuNr;
    }

}
