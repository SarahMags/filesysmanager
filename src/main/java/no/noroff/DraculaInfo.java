package main.java.no.noroff;

import java.io.File;
import java.util.Scanner;

public class DraculaInfo {
    static File dracula = new File("src/resources/dracula.txt"); //Saving the dracula txt file as a File object so it can be manipulated

    static void draculaName(){
        String draculaName = dracula.getName();
        System.out.println("The name of the Dracula file is: "+draculaName);
    }


    static void draculaSize(){
        long draculaSize = dracula.length() / 1024; //Divides by 1024 to convert size in bytes to KB
        System.out.println("The size of Dracula.txt is: "+draculaSize+"KB");
    }


    static void draculaLines() {
        try {
            int draculaLines = 0; //Counter variable for amount of lines

            Scanner lineScanner = new Scanner(dracula);
            while (lineScanner.hasNextLine()) { //Reads through file and increases counter every line
                lineScanner.nextLine();
                draculaLines++;
            }
            lineScanner.close();
            System.out.println("The number of lines in Dracula.txt is: " + draculaLines);
        }
        catch (Exception e) {
            e.getStackTrace();
        }
    }


}
