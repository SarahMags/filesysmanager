package main.java.no.noroff;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DraculaWordSearch {
    static int count = 0;
    static File dracula = new File("src/resources/dracula.txt"); //Saving the dracula txt file as a File object so it can be manipulated


    static String chosenWord(){ //Method to return the string that the user enters
        Scanner lineScanner = new Scanner(System.in);
        System.out.println("Enter single word to search for in Dracula.txt. Enter b to go back, or q to quit ");
        return lineScanner.next();
    }

    static void draculaWordExist(String word){
        if (!word.equals("b") && !word.equals("q")) { //As long as the input from user is not "b" (back) or "q" (quit)
            try {
                Scanner lineScanner = new Scanner(dracula).useDelimiter("(\\p{javaWhitespace}|\\p{Punct})"); //use both whitespace and special characters to separate words, to include the word even if it's next to e.g. a comma
                String nextWord;
                boolean containsWord = false;

                while (lineScanner.hasNext()) { //Read through file
                    nextWord = lineScanner.next();
                    if (nextWord.equalsIgnoreCase(word)) { //If the next word in file is equal to user input word
                        containsWord = true;
                    }
                }
                if (containsWord) {
                    System.out.println("dracula.txt contains the word " + word);
                } else {
                    System.out.println("The file dracula.txt does not contain the word " + word);
                }
                lineScanner.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    static void draculaWordCount(String word){
        try {
            Scanner lineScanner = new Scanner(dracula).useDelimiter("(\\p{javaWhitespace}|\\p{Punct})"); //use both whitespace and special characters to separate words, to include the word even if it's next to e.g. a comma

            while (lineScanner.hasNext()) { //Read through file
                String nextWord = lineScanner.next();
                if (nextWord.equalsIgnoreCase(word)){ //Increase counter if the next word in file is equal to user input word
                    count++;
                }

            }
            if (count > 0){
                System.out.println("dracula.txt contains the word "+word+" "+ count +" times.");
            }else{
                System.out.println("The file dracula.txt does not contain the word "+word);
            }
            lineScanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
