package main.java.no.noroff;

public class ExtensionOptions {
    static String extensionType(){ //Method called after choosing option "Print file names by extension", presents user with options and receives choice
        String extType;
        System.out.println("""
                Enter number to choose extension type:
                1:txt
                2:jpeg
                3:jpg
                4:png
                5:jfif
                6:Back to menu
                7:Quit""");

        do{
            while(!ConsoleMenu.ioScanner.hasNextInt()){
                System.out.println("Please enter a valid number.");
                ConsoleMenu.ioScanner.next(); //keeps the scanner going until it has the next valid int
            }
            ConsoleMenu.menuNr = ConsoleMenu.ioScanner.nextInt();
        } while(ConsoleMenu.menuNr < 0);

        if (ConsoleMenu.menuNr == 1){ //Returns String value corresponding to users numeral choice
            extType = ".txt";
        } else if (ConsoleMenu.menuNr == 2){
            extType = ".jpeg";
        }else if (ConsoleMenu.menuNr == 3){
            extType = ".jpg";
        }else if (ConsoleMenu.menuNr == 4){
            extType = ".png";
        }else if (ConsoleMenu.menuNr == 5){
            extType = ".jfif";
        }else if (ConsoleMenu.menuNr == 6){
            extType = "back";
        }else if (ConsoleMenu.menuNr == 7){
            extType = "quit";
        }else{
            extType = "back"; //Couldn't figure out how to handle numbers above 7, sends user back to avoid NullPointerException
        }

        return extType;
    }
}
