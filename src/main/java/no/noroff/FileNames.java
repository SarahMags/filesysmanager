package main.java.no.noroff;

import java.io.File;

public class FileNames {
    static File f = new File("src/resources");
    private static String[] pathNames = f.list(); //Makes array of files in directory from path in File f

    static void listFileNames(){ //Prints all files in resources folder
        for (String pathname : pathNames){
            System.out.println(pathname);
        }
    }


    static void listNamesByExtension(String extensionType){ //Prints all files with extension type chosen by user with extensionType() method in ExtensionOptions

        for (String pathname : pathNames){
            if(pathname.endsWith(extensionType)){
                System.out.println(pathname);
            }
        }
    }
}
