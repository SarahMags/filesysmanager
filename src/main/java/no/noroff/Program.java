package main.java.no.noroff;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

public class Program {

    public static void main(String[] args) {
        boolean loopMenu = true;

        while (loopMenu) { //While loop to keep progMenu() running after user makes choices
            String userWord;
            String logTxt;
            Date date = new Date();
            Timestamp ts;
            long startTime;
            long endTime;
            long duration;

            int menuNrChosen = ConsoleMenu.progMenu(); //progMenu() returns the users choice from the menu

            switch (menuNrChosen) {
                case 1 -> { //List all file names
                    ts = new Timestamp(date.getTime()); //Timestamp for log file
                    startTime = System.nanoTime();
                    FileNames.listFileNames(); //Runs method listFileNames() in Filenames class to print all file names
                    endTime = System.nanoTime();
                    duration = ((endTime - startTime) / 1000000); //Duration in ms, uses start and end times to calculate duration for log file
                    logTxt = ts + ": The function listFileNames() took " + duration + "ms to execute\n"; //Creates string for log file
                    writeToLog(logTxt); //method writeToLog() at the end of Program file appends logTxt string to file logFile in src/log package
                }
                case 2 -> { //List file names by extension
                    ts = new Timestamp(date.getTime());
                    startTime = System.nanoTime();
                    String extensionType = ExtensionOptions.extensionType(); //Runs method in ExtensionOption to receive option choice from user
                    if (extensionType != null && !extensionType.equals("quit") && !extensionType.equals("back")) { //if user chose an extension, and not to go back or quit
                        FileNames.listNamesByExtension(extensionType); //method prints files that matches users choice of extension type
                        endTime = System.nanoTime();
                        duration = ((endTime - startTime) / 1000000);
                        logTxt = ts + ": The function listNamesByExtension() took " + duration + "ms to execute\n";
                        writeToLog(logTxt);
                    } else {
                        assert extensionType != null; //To avoid NullPointerException, ended up usingS "back" as default instead of null in extensionType(), so extensionType will always be true
                        if (extensionType.equals("quit")) {
                            loopMenu = false;
                        }
                    }
                }
                case 3 -> { //Print name of dracula.txt file
                    ts = new Timestamp(date.getTime());
                    startTime = System.nanoTime();
                    DraculaInfo.draculaName(); //Method draculaName() from DraculaInfo class, prints name of file
                    endTime = System.nanoTime();
                    duration = ((endTime - startTime) / 1000000);
                    logTxt = ts + ": The function draculaName() took " + duration + "ms to execute\n";
                    writeToLog(logTxt);
                }
                case 4 -> { //Print size of dracula.txt
                    ts = new Timestamp(date.getTime());
                    startTime = System.nanoTime();
                    DraculaInfo.draculaSize(); //Method draculaSize() from DraculaInfo class, prints size of file
                    endTime = System.nanoTime();
                    duration = ((endTime - startTime) / 1000000);
                    logTxt = ts + ": The function draculaSize() took " + duration + "ms to execute\n";
                    writeToLog(logTxt);
                }
                case 5 -> { //Print amount of lines in dracula.txt
                    ts = new Timestamp(date.getTime());
                    startTime = System.nanoTime();
                    DraculaInfo.draculaLines(); //Method draculaLines() from DraculaInfo class, prints number of lines in file
                    endTime = System.nanoTime();
                    duration = ((endTime - startTime) / 1000000);
                    logTxt = ts + ": The function draculaLines() took " + duration + "ms to execute\n";
                    writeToLog(logTxt);
                }

                case 6 -> { //Search if word exists in dracula.txt
                    userWord = DraculaWordSearch.chosenWord(); //Method chosenWord() from DraculaWordSearch class, gets chosen word from user
                    if (userWord != null && !userWord.equals("b") && !userWord.equals("q")) {
                        ts = new Timestamp(date.getTime());
                        startTime = System.nanoTime();
                        DraculaWordSearch.draculaWordExist(userWord); //Method draculaWordExist() from DraculaWordSearch class, prints if word exists in file
                        endTime = System.nanoTime();
                        duration = ((endTime - startTime) / 1000000);
                        logTxt = ts + ": The function draculaWordExist() took " + duration + "ms to execute\n";
                        writeToLog(logTxt);
                    } else {
                        assert userWord != null;
                        if (userWord.equals("q")) { //Quits program if user input is "q"
                            loopMenu = false;
                        }
                    }
                }
                case 7 -> { //Search how many times specific word appears in dracula.txt
                    userWord = DraculaWordSearch.chosenWord(); //Gets word from user
                    if (userWord != null && !userWord.equals("b") && !userWord.equals("q")) {
                        ts = new Timestamp(date.getTime());
                        startTime = System.nanoTime();
                        DraculaWordSearch.draculaWordCount(userWord); //Method draculaWordCount() from DraculaWordSearch class, prints how many times word appears in file
                        endTime = System.nanoTime();
                        duration = ((endTime - startTime) / 1000000);
                        logTxt = ts + ": The function draculaWordSearch() took " + duration + "ms to execute\n";
                        writeToLog(logTxt);
                    } else {
                        assert userWord != null;
                        if (userWord.equals("q")) { //Quits program if user input is "q"
                            loopMenu = false;
                        }
                    }
                }
                case 8 -> loopMenu = false; //Quit
                default -> System.out.println("Please enter a number between 1 and 7."); //If input is not between 1-7, ask for new number
            }

        }
    }

    public static void writeToLog(String logTxt) { //Method to append timestamp and duration of method call to logFile.txt in src/log
        File logFile = new File("C:\\Users\\smthor\\Desktop\\JavaLessons\\FileSysManager\\src\\log\\logFile.txt");
        try {
            FileWriter writeLog = new FileWriter(logFile, true); //Creates log file if it doesn't exist, appends if write() method is used
            writeLog.write(logTxt); //Appends string to logFile.txt
            writeLog.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

